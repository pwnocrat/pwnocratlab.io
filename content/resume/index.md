---
title: "Resume"
date: 2020-12-14
description: "Resume"
ignore_footer: true
---

## Work Experience

**Security Intern** @CYR3CON | *June 2020 - Present*
- Created a tool using bash for checking and enforcing CIS compliance for Ubuntu machines.
- Penetration testing of web applications,patching in AWS,log analysis using Cloudtrail, guardduty.
- Worked on creating testcases for open source SIEM Wazuh and endpoint security application YARA on AWS platform.
- Docker CIS compliance and docker security.
- Creating and patching CI/CD pipelines using gitlab for DevOps.

**Security Engineer** @SIFY TECHNOLOGIES | *Jul 2017- Jul 2019*
- Worked in the defensive operations of the SOC handling Firewalls, OS Hardening, and log analysis.
- Created numerous use cases using Indicators of compromise to mitigate security breaches.
- Integrated threat intelligence framework STIX/TAXII with SIEM-Arcsight

## Projects

**Secure Bank App**
- Spearheaded a group of 9 people to build a web application that performs banking functions securely.
- Built the security modules of the project including Public Key Infrastructure(PKI), Two Factor Authentication(2FA),
Data masking, Database entry obfuscation, hashing critical PII (Personally identifiable information) and credentials.

**Pentesting Testbed Environment**
- Developed a testbed environment for penetration testing and SIEM, which performs real time simulation of attacks.
- Analyzed logs using ArcSight to devise real time rules for SIEM to prevent security breaches.

**Campus Virtual Tour**
- Implemented User Interface components in Unity with a focus on reusability and consistency
- Developed a RESTful web service for retrieval of campus building information

## Skills

**Programming** : Python, bash, Java, Powershell, , C, C++\
**Security** : Metasploit, Wireshark, Nmap, IDA Pro\
**Cloud & Containers** : AWS, Azure, Docker\
**Frameworks** : Django, Flask\

## Education
**Masters in Software Engineering(Cyber security)** @Arizona State University, Tempe \
**Bachelors in ECE** @Anna University, Chennai
