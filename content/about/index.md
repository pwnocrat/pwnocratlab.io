---
title: "About"
date: 2020-12-14T17:13:47+05:30
draft: false
---

Hello there, I am Madhu Madhavan Sridhar. I am a security researcher pursuing masters in software engineering at ASU with focus on cyber security. I strive to learn and become an application security engineer. I have 2 years of experience as a security engineer with experience on SIEM and Incident response. Currently, I work as an security intern with primary focus on CIS compliance, penetration testing and docker security. I love participating in hackathons and cyber security competitions like capture the flags, wargames. I have played Overthewire, hack the box and a number of CTFs.
